# Desafío técnico Mantenedor N°1

## Version
-   Spring Boot 2.5.2

## Requerimientos Minimos
-   Java 1.8
-   Maven 3.0+
-   MySql 5.0.12

## Deploy

1.   Ejecutar servidor de base de datos MySql

2.   Editar el usuario y password con acceso de lectura y escritura a la base de datos en las siguientes lineas

        -   spring.datasource.url= jdbc:mysql://localhost:3306/desafio_tecnico?useSSL=false
        -   spring.datasource.username= root
        -   spring.datasource.password= 123456

ubicados en la siguiente ruta 

        \src\main\resources\application.properties
 
3.   Abrir terminal en la ruta principal del proyecto
4.   Ejecutar el siguiente comando

            mvn spring-boot:run

        ### ***Nota Importante:*** 
        La aplicacion posee un script para la autopoblacion de datos para prueba, si no se desea usar estos datos se debe acceder a la siguiente ruta

            \src\main\resources\ 

        y eliminar el archivo **data.sql** antes de su ejecucion 

## Testing

1. una vez ejecutado el sistema se genera documentacion y testing con Swagger, se puede consultar a traves de la URL http://localhost:8080/swagger-ui.html

2. Se anexa en la ruta principal del proyecto archivo para importacion de prueba de endpoints y Test en programa POSTMAN 

        BACKEND-TAREAS.postman_collection.json
        BACKEND-TAREAS TEST.postman_collection.json

## Intrucciones

### Crear una aplicación Web [front - back] que permita realizar el registro de tareas, con las siguientes funcionalidades:

-	Listar tareas
-	Agregar una tarea
-	Remover una tarea
-	Editar una tarea 

## Consideraciones

### Front // No realizar
-	Utilizar las librerías React + Redux
-	Validar que los controles de entrada sean obligatorios
-	Crear pruebas unitarias

### Back
-	Construir utilizando el framework Spring Boot
-	Exponer mediante API REST
-	Utilizar Swagger para documentar las API
-	Validar que en las acciones agregar/editar sean obligatorios los campos de entrada
-	Utilizar para la persistencia lo que más le acomode [ORM o nativo JDBC]
-	Crear pruebas unitarias

### DB
-	Utilizar el motor DB que más le acomode [relacional, NoSql]
-	La estructura de la tarea deberá contener los siguientes campos: identificador[numérico], descripción [cadena], fechaCreación [fecha tiempo], vigente [booleano]

#### Entrega
-	Compartir en un repositorio git el código fuente del front, back y script de base de datos. Además crear instructivo que indique como desplegar el aplicativo.
