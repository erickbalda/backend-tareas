CREATE TABLE IF NOT EXISTS tareas (
  id INT NOT NULL AUTO_INCREMENT,
  descripcion VARCHAR(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  fechacreacion TIMESTAMP DEFAULT current_timestamp(),
  vigencia tinyint(1) NOT NULL,
  PRIMARY KEY (id));
  