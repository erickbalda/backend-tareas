package cl.erickbalda.tareas.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.erickbalda.tareas.model.Tareas;
import cl.erickbalda.tareas.repository.TareasRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping("v1/api")
public class TareasController {

    @Autowired
    TareasRepository tareasRepository;

    /**
     * Listado de todas las tareas
     */
    @ApiOperation(value = "getAllTareas", notes = "Listado de todas las tareas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = Tareas.class),
            @ApiResponse(code = 400, message = "Bad Request. Parametro invalido", response = Tareas.class), })
    @GetMapping("/tareas")
    public ResponseEntity<List<Tareas>> getAllTareas() {
        try {
            List<Tareas> tareas = tareasRepository.findAll();

            if (tareas.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(tareas, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Listado de tarea segun id
     */
    @ApiOperation(value = "getTareaById", notes = "Listado de tarea segun id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = Tareas.class),
            @ApiResponse(code = 400, message = "Bad Request. Parametro invalido", response = Tareas.class),
            @ApiResponse(code = 405, message = "Metodo no permitido", response = Tareas.class) })
    @GetMapping("/tareas/{id}")
    public ResponseEntity<Tareas> getTareaById(@PathVariable("id") Integer id) {
        Optional<Tareas> tareasData = tareasRepository.findById(id);

        if (tareasData.isPresent()) {
            return new ResponseEntity<>(tareasData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Guarda nueva tarea
     */
    @ApiOperation(value = "createTarea", notes = "Guarda nueva tarea")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Tarea creada con exito", response = Tareas.class),
            @ApiResponse(code = 400, message = "Bad Request. Parametro invalido", response = Tareas.class),
            @ApiResponse(code = 405, message = "Metodo no permitido", response = Tareas.class) })
    @PostMapping("/tareas")
    public ResponseEntity<Tareas> createTarea(@Valid @RequestBody Tareas tareas) {
        try {
            Tareas _tareas = tareasRepository.save(new Tareas(0, tareas.getDescripcion(), null, tareas.isVigencia()));
            return new ResponseEntity<>(_tareas, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Edita tarea segun id
     */
    @ApiOperation(value = "updateTareaById", notes = "Edita tarea segun id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. El recurso se obtiene correctamente", response = Tareas.class),
            @ApiResponse(code = 400, message = "Bad Request. Parametro invalido", response = Tareas.class),
            @ApiResponse(code = 405, message = "Metodo no permitido", response = Tareas.class) })
    @PutMapping("/tareas/{id}")
    public ResponseEntity<Tareas> updateTareaById(@PathVariable("id") Integer id,
            @Valid @RequestBody @Validated Tareas tareas) {
        Optional<Tareas> tareasData = tareasRepository.findById(id);

        if (tareasData.isPresent()) {
            Tareas _tareas = tareasData.get();
            _tareas.setDescripcion(tareas.getDescripcion());
            _tareas.setVigencia(tareas.isVigencia());
            return new ResponseEntity<>(tareasRepository.save(_tareas), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Elimina tarea segun id
     */
    @ApiOperation(value = "deleteTareaById", notes = "Elimina tarea segun id")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Tarea eliminada con exito", response = Tareas.class),
            @ApiResponse(code = 405, message = "Metodo no permitido", response = Tareas.class) })
    @DeleteMapping("/tareas/{id}")
    public ResponseEntity<HttpStatus> deleteTareaById(@PathVariable("id") Integer id) {
        try {
            tareasRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
