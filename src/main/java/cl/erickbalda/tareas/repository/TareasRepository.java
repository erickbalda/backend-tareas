package cl.erickbalda.tareas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.erickbalda.tareas.model.Tareas;

public interface TareasRepository extends JpaRepository<Tareas, Integer> {

  }