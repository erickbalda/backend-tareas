package cl.erickbalda.tareas.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "tareas")
public class Tareas {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "descripcion", nullable = false)
    @NotNull(message = "Por favor introduzca descripcion de tarea")
    @NotBlank(message = "La descripcion no puede estar vacia")
    private String descripcion;

    @Column(name = "fechacreacion", nullable = false, updatable = false)
    @CreationTimestamp
    private Date fechacreacion;

    @Column(name = "vigencia")
    private boolean vigencia;

    public Tareas(int id, String descripcion, Date fechacreacion, boolean vigencia) {
        this.id = id;
        this.descripcion = descripcion;
        this.fechacreacion = fechacreacion;
        this.vigencia = vigencia;
    }

    public Tareas() {
    }

    @Override
    public String toString() {
        return "Tareas [descripcion=" + descripcion + ", fechacreacion=" + fechacreacion + ", id=" + id + ", vigencia="
                + vigencia + "]";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public boolean isVigencia() {
        return vigencia;
    }

    public void setVigencia(boolean vigencia) {
        this.vigencia = vigencia;
    }

}
